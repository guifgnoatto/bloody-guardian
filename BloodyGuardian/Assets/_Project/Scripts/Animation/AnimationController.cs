﻿using UnityEngine;

namespace BloodyGuardian.Animation
{
    public class AnimationController : MonoBehaviour
    {
        private string _currentParameter;
        private Animator _animator;

        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _animator = GetComponent<Animator>();
        }

        public void StartAnimation(string parameter)
        {
            if (_currentParameter != null)
            {
                SetAnimation(_currentParameter, false);
            }

            SetAnimation(parameter, true);
            _currentParameter = parameter;
        }

        private void SetAnimation(string parameter, bool condition)
        {
            _animator.SetBool(parameter, condition);
        }
    }
}
