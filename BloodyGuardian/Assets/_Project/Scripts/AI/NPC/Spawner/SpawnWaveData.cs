﻿using System.Collections.Generic;
using BloodyGuardian.IA;
using UnityEngine;

namespace BloodyGuardian.Spawner
{
    [CreateAssetMenu(fileName = "SpawnWave Data", menuName = "NPC/SpawnWaveData")]
    public class SpawnWaveData : ScriptableObject
    {
        [SerializeField]
        private float _timeForEachSpawn = default;
        [SerializeField]
        private List<NPCBase> _npcToSpawn = default;
        
        public float TimeForEachSpawn
        {
            get { return _timeForEachSpawn; }
        }
        
        public List<NPCBase> NpcToSpawn
        {
            get { return _npcToSpawn; }
        }
    }
}