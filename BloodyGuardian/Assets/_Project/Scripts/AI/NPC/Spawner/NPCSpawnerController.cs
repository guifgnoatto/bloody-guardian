﻿using System;
using System.Collections;
using System.Collections.Generic;
using BloodyGuardian.IA;
using BloodyGuardian.Pool;
using BloodyGuardian.Event;
using BloodyGuardian.Manager;
using UnityEngine;
using Random = System.Random;

namespace BloodyGuardian.Spawner
{
	public class NPCSpawnerController : MonoBehaviour
	{
		[SerializeField]
		private List<SpawnWaveData> _spawnWaves = default;
		[SerializeField]
		private GameEvents _gameEvents = default;

		private int _actualWaveIndex = 0;
		private int _actualWaveNPCIndex = 0;
		private float _actualWaveTimer;

		private void OnEnable()
		{
			RegisterToEvents();
		}

		private void OnDisable()
		{
			UnregisterFromEvents();
		}

		private void RegisterToEvents()
		{
			if (_gameEvents != null)
			{
				_gameEvents.GameStarted += StartSpawn;
			}
		}

		private void UnregisterFromEvents()
		{
			_gameEvents.GameStarted -= StartSpawn;
		}

		private void StartSpawn()
		{
			_actualWaveTimer = _spawnWaves[_actualWaveIndex].TimeForEachSpawn;
			StartCoroutine(SpawWaveByTimer());
		}

		private IEnumerator SpawWaveByTimer()
		{
			SpawnNPC(_spawnWaves[_actualWaveIndex].NpcToSpawn[_actualWaveNPCIndex]);
			yield return new WaitForSeconds(_actualWaveTimer);

			if (WaveHasFinished())
			{
				SpawnNextWave();
			}
			else
			{
				SpawnNextNPC();
			}
		}

		private void SpawnNPC(NPCBase npc)
		{
			NPCManager.Instance.SpawnNPC(npc, transform.position, Quaternion.identity);
		}

		private bool WaveHasFinished()
		{
			if (_actualWaveNPCIndex < _spawnWaves[_actualWaveIndex].NpcToSpawn.Count - 1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		private void SpawnNextWave()
		{
			if (_actualWaveIndex == _spawnWaves.Count - 1)
			{
				NPCManager.Instance.WavesEnded = true;
			}
			else
			{
				_actualWaveNPCIndex = 0;
				_actualWaveIndex++;
				StartSpawn();
			}
		}

		private void SpawnNextNPC()
		{
			_actualWaveNPCIndex++;
			StartCoroutine(SpawWaveByTimer());
		}

	}
}