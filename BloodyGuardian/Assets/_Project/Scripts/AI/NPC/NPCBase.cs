﻿using System;
using System.Collections;
using BloodyGuardian.Collision;
using BloodyGuardian.HUD;
using UnityEngine;
using BloodyGuardian.Manager;
using BloodyGuardian.Particle;

namespace BloodyGuardian.IA
{
	public class NPCBase : MonoBehaviour
	{
		public NPCAttributesData Attributes;

		[SerializeField]
		protected ParticleHandler _particleHandler = default;

		protected bool _isAlive = true;

		public virtual void TakeDamage()
		{
			_particleHandler.Play();
		}

		public virtual void OnUpdate()
		{ }

		protected virtual void Die()
		{
			_isAlive = false;
		}

		protected virtual void ReachTemple()
		{ }

		protected void Run(float speed, Transform myTransform)
		{
			myTransform.position += new Vector3(0, 0, speed) * Time.deltaTime;
		}

		protected IEnumerator FootSteps()
        {
			while (_isAlive)
            {
				yield return new WaitForSeconds(.3f);
				SoundManager.Instance.PlayFootStepSound();
            }
        }

		private void OnTriggerEnter(Collider other)
		{
			ReachTemple();
		}
	}
}