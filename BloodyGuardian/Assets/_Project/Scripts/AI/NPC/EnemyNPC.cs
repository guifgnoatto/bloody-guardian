﻿using System.Collections;
using BloodyGuardian.Animation;
using BloodyGuardian.Event;
using BloodyGuardian.Pool;
using BloodyGuardian.Manager;
using UnityEngine;

namespace BloodyGuardian.IA
{
    public class EnemyNPC : NPCBase, IPooledObject
    {
        [SerializeField]
        private AnimationController _animationController = default;
        
        private int _actualLife;
        private float _actualSpeed;
        private Transform _transform;
        
        private readonly float _deathDelay = 2;
        private readonly string _runAnimation = "isRunning";
        private readonly string _dieAnimation = "isDying";

        public override void TakeDamage()
        {
            base.TakeDamage();
            _actualLife -= 1;
            if (_actualLife==0)
            {
                Die();
            }
        }
        
        public override void OnUpdate()
        {
            if (_isAlive)
            {
                Run(_actualSpeed,_transform);   
            }
        }
        
        public void OnObjectSpawn()
        {
            InitializeAttributes();
            NPCManager.Instance.AddNPC(this);
            _animationController.StartAnimation(_runAnimation);
            StartCoroutine(FootSteps());
        }

        protected override void Die()
        {
            base.Die();
            StartCoroutine(DieAnimation());
            
            IEnumerator DieAnimation()
            {
                _animationController.StartAnimation(_dieAnimation);
                yield return new WaitForSeconds(_deathDelay);
                NPCManager.Instance.NPCDieHandler(this);
            }
        }
        
        protected override void ReachTemple()
        {
            GameEvents.Instance.OnEnemyReachTemple();
            NPCManager.Instance.NPCReachTempleHandler(this);
        }
        
        private void Awake()
        {
            _transform = transform;
            _animationController = GetComponent<AnimationController>();
        }

        private void InitializeAttributes()
        {
            _actualSpeed = Attributes.Speed;
            _actualLife = Attributes.MaxLife;
            _isAlive = true;
        }
    }
}