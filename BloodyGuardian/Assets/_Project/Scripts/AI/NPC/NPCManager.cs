﻿using System;
using System.Collections.Generic;
using BloodyGuardian.Event;
using BloodyGuardian.Pool;
using UnityEngine;
using BloodyGuardian.IA;

namespace BloodyGuardian.Manager
{
	public class NPCManager : MonoBehaviour
	{
		public static NPCManager Instance;

		private List<NPCBase> _npcs;
		private bool _wavesEnded = false;

		public void AddNPC(NPCBase npc)
		{
			_npcs.Add(npc);
		}

		public void SpawnNPC(NPCBase npc, Vector3 spawnPosition, Quaternion rotation)
		{
			ObjectPooler.Instance.SpawnFromObjectPool(npc.Attributes.Prefab, spawnPosition, rotation);
		}

		public void NPCDieHandler(NPCBase npc)
		{
			npc.gameObject.SetActive(false);
			RemoveNPC(npc);
		}

		public void NPCReachTempleHandler(NPCBase npc)
		{
			npc.gameObject.SetActive(false);
			RemoveNPC(npc);
		}

		private void Awake()
		{
			Initialize();
		}

		private void Update()
		{
			NpcUpdate();

			if (_wavesEnded == true)
			{
				TestGameIsOver();
			}
		}

		private void Initialize()
		{
			if (Instance == null)
			{
				Instance = this;
			}
			else
			{
				Destroy(gameObject);
			}
			_npcs = new List<NPCBase>();
		}

		private void NpcUpdate()
		{
			foreach (NPCBase npc in _npcs)
			{
				npc.OnUpdate();
			}
		}

		private void TestGameIsOver()
		{
			if (_npcs.Count == 0)
			{
				GameEvents.Instance.EndGameWin();
			}
		}

		private void RemoveNPC(NPCBase npc)
		{
			_npcs.Remove(npc);
		}

		public bool WavesEnded
		{
			set => _wavesEnded = value;
		}
	}
}