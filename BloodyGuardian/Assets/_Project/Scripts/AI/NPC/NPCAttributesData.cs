﻿using BloodyGuardian.Pool;
using UnityEngine;

namespace BloodyGuardian.HUD
{
	[CreateAssetMenu(fileName = "NPCAttributes Data", menuName = "NPC/NPCAttributesData")]
	public class NPCAttributesData : ScriptableObject
	{
		[SerializeField]
		private int _maxLife = default;
		[SerializeField]
		private float _speed = default;
		[SerializeField]
		private ObjectToPool _prefab = default;
		
		public float Speed
		{
			get { return _speed; }
		}
		
		public int MaxLife
		{
			get { return _maxLife; }
		}

		public ObjectToPool Prefab
		{
			get { return _prefab; }
		}
	}
}