﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BloodyGuardian.Player
{
    public class NinjaBehaviour : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _smokeParticle = default;
        [SerializeField] private GameObject _ninjaModel = default;
        [SerializeField] private float _respawnTime = default;

        private PlayerControls _playerControls;
        private IEnumerator _backToPlaceTimer;

        private void Awake()
        {
            Initialize();
        }

        private void OnEnable()
        {
            ActivateNinja();
        }

        private void OnDisable()
        {
            _playerControls.Disable();
        }

        private void ActivateNinja()
        {
            if (_ninjaModel.activeSelf == false)
            {
                _ninjaModel.SetActive(true);
                StopAllCoroutines();
            }
            _playerControls.Enable();
        }

        private void Initialize()
        {
            _playerControls = new PlayerControls();
            _playerControls.KillNpc.MouseClick.performed += ctx => Attack();
            _playerControls.KillNpc.ScreenTap.performed += ctx => Attack();
            _playerControls.Enable();
        }

        private void Attack()
        {
            if (_ninjaModel.activeSelf == true)
            {
                _smokeParticle.Play();
                _ninjaModel.SetActive(false);
                _backToPlaceTimer = BackToPlaceTimer();
            }
            StopCoroutine(_backToPlaceTimer);
            _backToPlaceTimer = BackToPlaceTimer();
            StartCoroutine(_backToPlaceTimer);
        }

        private void RespawnNinja()
        {
            _ninjaModel.SetActive(true);
        }

        private IEnumerator BackToPlaceTimer()
        {
            yield return new WaitForSeconds(_respawnTime);
            RespawnNinja();
        }
    }
}