﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using BloodyGuardian.Collision;
using BloodyGuardian.Manager;
using BloodyGuardian.Camera;

namespace BloodyGuardian.Player
{
    public class DamageController : MonoBehaviour
    {
        [SerializeField]
        private UnityEngine.Camera _mainCamera = default;

        private PlayerControls _playerControls;
        private CameraShake _cameraShake;
        private GameObject _mouseOverObject;

        private void Awake()
        {
            Initialize();
        }

        private void OnEnable()
        {
            RegisterToEvents();
        }

        private void OnDisable()
        {
            UnregisterToEvents();
        }

        private void Initialize()
        {
            _cameraShake = _mainCamera.GetComponent<CameraShake>();

            _playerControls = new PlayerControls();
            _playerControls.KillNpc.MouseClick.performed += ctx => Click(false);
            _playerControls.KillNpc.ScreenTap.performed += ctx => Click(true);
            _playerControls.KillNpc.MouseClick.Enable();
            _playerControls.KillNpc.ScreenTap.Enable();
        }

        private void RegisterToEvents()
        {
            _playerControls.KillNpc.MouseClick.Enable();
            _playerControls.KillNpc.ScreenTap.Enable();
        }

        private void UnregisterToEvents()
        {
            _playerControls.KillNpc.MouseClick.Disable();
            _playerControls.KillNpc.ScreenTap.Disable();
        }

        private void Click(bool mobile)
        {
            Vector2 clickPosition;
            if (!mobile)
            {
                clickPosition = Mouse.current.position.ReadValue();
            }
            else
            {
                clickPosition = Touchscreen.current.position.ReadValue();
            }

            GetObjectClicked(clickPosition);
            if (_mouseOverObject != null &&
                _mouseOverObject.TryGetComponent<ICollideable>(out ICollideable npc))
            {
                DealDamage(npc);
            }
            else
            {
                MissClick();
            }
        }

        private void DealDamage(ICollideable npc)
        {
            npc.CollideHandler();
            SoundManager.Instance.PlayDamageSound();
            if (gameObject.activeSelf)
            {
                StartCoroutine(_cameraShake.Shake());
            }
        }

        private void MissClick()
        {
            SoundManager.Instance.PlayMissSound();
        }

        private void GetObjectClicked(Vector2 clickPosition)
        {
            Ray ray = _mainCamera.ScreenPointToRay(clickPosition); //Mouse.current.position.ReadValue());
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                _mouseOverObject = hit.collider.gameObject;
            }
        }
    }
}