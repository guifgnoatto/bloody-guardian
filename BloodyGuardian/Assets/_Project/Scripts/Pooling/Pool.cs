﻿using UnityEngine;

namespace BloodyGuardian.Pool
{
    [System.Serializable]
    public class Pool
    {
        public ObjectToPool ObjectName = default;
        public GameObject Prefab = default;
        public int Size = default;
    }
}