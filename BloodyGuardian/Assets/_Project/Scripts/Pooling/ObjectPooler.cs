﻿using System.Collections.Generic;
using UnityEngine;

namespace BloodyGuardian.Pool
{
    public class ObjectPooler : MonoBehaviour
    {
        public static ObjectPooler Instance;

        public List<Pool> pools;
        private Dictionary<ObjectToPool, Queue<GameObject>> _poolDictionary;

        public GameObject SpawnFromObjectPool(ObjectToPool objectToPool, Vector3 position, Quaternion rotation)
        {
            GameObject objectToSpawn = _poolDictionary[objectToPool].Dequeue();
            Transform objectTransform = objectToSpawn.transform;

            objectTransform.position = position;
            objectTransform.rotation = rotation;
            objectToSpawn.SetActive(true);
            
            if (objectToSpawn.TryGetComponent<IPooledObject>(out IPooledObject pooledObj))
            {
                pooledObj.OnObjectSpawn();
            }

            _poolDictionary[objectToPool].Enqueue(objectToSpawn);

            return objectToSpawn;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
            
            LoadDictionary();
        }

        private void LoadDictionary()
        {
            _poolDictionary = new Dictionary<ObjectToPool, Queue<GameObject>>();

            foreach (Pool pool in pools)
            {
                Queue<GameObject> objectPool = new Queue<GameObject>();

                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject obj = Instantiate(pool.Prefab,transform);
                    obj.SetActive(false);
                    objectPool.Enqueue(obj);
                }

                _poolDictionary.Add(pool.ObjectName, objectPool);
            }
        }
    }
}