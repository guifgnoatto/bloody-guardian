﻿namespace BloodyGuardian.Pool
{
    public interface IPooledObject
    {
        void OnObjectSpawn();
    }
}
