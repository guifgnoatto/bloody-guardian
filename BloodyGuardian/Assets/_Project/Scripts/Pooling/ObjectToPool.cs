﻿namespace BloodyGuardian.Pool
{
    public enum ObjectToPool
    {
        NPC_ALLY_SAMURAI = 0,
        NPC_ALLY_SAMURAI_BLACK = 1,
        NPC_ALLY_GEISHA = 2,
        NPC_ENEMY_CULT = 3,
    }
}
