﻿using System;
using UnityEngine;

namespace BloodyGuardian.Event
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents Instance;

        public event Action GameStarted;
        public event Action GamePaused;
        public event Action GameResumed;
        public event Action GameEndedWin;
        public event Action GameEndedLose;
        public event Action EnemyReachedTemple;
        public event Action AllyReachedTemple;

        public void EndGameWin()
        {
            if (GameEndedWin != null)
            {
                GameEndedWin();
            }
        }
        public void EndGameLose()
        {
            if (GameEndedLose != null)
            {
                GameEndedLose();
            }
        }

        public void PauseGame()
        {
            if (GamePaused != null)
            {
                GamePaused();
            }
        }

        public void ResumeGame()
        {
            if (GameResumed != null)
            {
                GameResumed();
            }
        }

        public void StartGame()
        {
            if (GameStarted != null)
            {
                GameStarted();
            }
        }

        public void OnEnemyReachTemple()
        {
            if (EnemyReachedTemple != null)
            {
                EnemyReachedTemple();
            }
        }

        public void OnAllyReachTemple()
        {
            if (AllyReachedTemple != null)
            {
                AllyReachedTemple();
            }
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Destroy(gameObject);
            }
            Instance = this;

            DontDestroyOnLoad(gameObject);
        }
    }
}