﻿using System;
using System.Diagnostics;
using BloodyGuardian.Event;
using BloodyGuardian.Manager;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

namespace BloodyGuardian.Gameplay
{
    public class TempleLifeController : MonoBehaviour
    {
        [SerializeField]
        private Slider _healthBarFill = default;
        [SerializeField]
        private int _maxLife = 10;
        [SerializeField]
        private int life = 5;

        private void Start()
        {
            Initalize();
        }

        private void OnDestroy()
        {
            GameEvents.Instance.EnemyReachedTemple -= Decrase;
            GameEvents.Instance.AllyReachedTemple -= Increase;
        }

        private void Initalize()
        {
            _healthBarFill.maxValue = _maxLife;
            _healthBarFill.value = life;

            GameEvents.Instance.EnemyReachedTemple += Decrase;
            GameEvents.Instance.AllyReachedTemple += Increase;
        }
        
        private void Increase()
        {
            if (life < _maxLife)
            {
                life++;
            }
            UpdateHealthBar();
        }

        private void Decrase()
        {
            life -= 2;
            if (life <= 0)
            {
                GameEvents.Instance.EndGameLose();
            }

            UpdateHealthBar();
        }

        private void UpdateHealthBar()
        {
            _healthBarFill.value = life;
        }
    }
}