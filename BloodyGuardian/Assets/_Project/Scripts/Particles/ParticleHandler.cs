﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BloodyGuardian.Particle
{
    public class ParticleHandler : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> _particles = default;

        public void Play()
        {
            foreach (ParticleSystem particle in _particles)
            {
                particle.Play();
            }
        }
    }
}