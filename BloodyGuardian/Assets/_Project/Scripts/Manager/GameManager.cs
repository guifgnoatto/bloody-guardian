﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BloodyGuardian.Event;
using BloodyGuardian.Player;

namespace BloodyGuardian.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private DamageController _playerDamageController = default;
        [SerializeField]
        private int _defaultWidth= 1920;
        [SerializeField]
        private int _defaultHeight= 1080;

        private void Start()
        {
            RegisterToEvents();
            Initialize();
        }

        private void OnDestroy()
        {
            UnregisterFromEvents();
        }

        private void RegisterToEvents()
        {
            GameEvents.Instance.GamePaused += PauseGame;
            GameEvents.Instance.GameEndedWin += PauseGame;
            GameEvents.Instance.GameEndedLose += PauseGame;

            GameEvents.Instance.GameResumed += ResumeGame;
        }

        private void UnregisterFromEvents()
        {
            GameEvents.Instance.GamePaused -= PauseGame;
            GameEvents.Instance.GameEndedWin -= PauseGame;
            GameEvents.Instance.GameEndedLose -= PauseGame;

            GameEvents.Instance.GameResumed -= ResumeGame;
        }

        private void Initialize()
        {
            Screen.SetResolution(_defaultWidth, _defaultHeight, true);
            GameEvents.Instance.StartGame();
        }

        private void PauseGame()
        {
            _playerDamageController.gameObject.SetActive(false);
            Time.timeScale = 0;
        }

        private void ResumeGame()
        {
            Time.timeScale = 1;
            _playerDamageController.gameObject.SetActive(true);
        }     
    }
}