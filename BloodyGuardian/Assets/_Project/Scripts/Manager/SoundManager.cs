﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Triplano.assets.SoundBox;
using BloodyGuardian.Event;

namespace BloodyGuardian.Manager
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Instance;

        [SerializeField]
        private SoundBox _soundboxKill = default;
        [SerializeField]
        private SoundBox _soundboxMiss = default;
        [SerializeField] 
        private SoundBox _soundboxTempleDamage = default;
        [SerializeField] 
        private SoundBox _soundboxTempleHeal = default;
        [SerializeField] 
        private SoundBox _soundboxFootStep= default;
        [SerializeField]
        private SoundBox _soundboxAmbient = default;

        public void PlayDamageSound()
        {
            _soundboxKill.PlaySound();
        }

        public void PlayMissSound()
        {
            _soundboxMiss.PlaySound();
        }

        public void PlayFootStepSound()
        {
            _soundboxFootStep.PlaySound();
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
            
        }

        private void OnEnable()
        {
            RegisterToEvents();
        }

        private void OnDestroy()
        {
            UnregisterToEvents();
        }

        private void RegisterToEvents()
        {
            GameEvents.Instance.GameStarted += PlayAmbientSound;
            GameEvents.Instance.EnemyReachedTemple += PlayTempleDamageSound;
            GameEvents.Instance.AllyReachedTemple += PlayTempleHealSound;
        }
        private void UnregisterToEvents()
        {
            GameEvents.Instance.GameStarted -= PlayAmbientSound;
            GameEvents.Instance.EnemyReachedTemple -= PlayTempleDamageSound;
            GameEvents.Instance.AllyReachedTemple -= PlayTempleHealSound;
        }

        private void PlayTempleDamageSound()
        {
            _soundboxTempleDamage.PlaySound();
        }

        private void PlayTempleHealSound()
        {
            _soundboxTempleHeal.PlaySound();
        }

        private void PlayAmbientSound()
        {
            _soundboxAmbient.PlaySound();
        }
    }
}