﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using BloodyGuardian.Event;
using BloodyGuardian.Player;
using UnityEngine.UI;
using UnityEngine;

namespace BloodyGuardian.UI
{
    public class HUDManager : MonoBehaviour
    {
        [SerializeField]
        private List<HUDController> _gameHuds = default;
        [SerializeField] private Animator _damageVignette = default;
        
        private readonly string _damageVignetteTrigger = "Blink";
        private readonly int _menuIndex = 0;
        private readonly int _gameIndex = 1;

        private void Start()
        {
            OnInitialize();
        }

        private void OnDestroy()
        {
            GameEvents.Instance.GameEndedWin -= StartWinScreen;
            GameEvents.Instance.GameEndedLose -= StartLoseScreen;
        }

        public void StartInGameHUD()
        {
            GameEvents.Instance.ResumeGame();
            ActivateHUDS(HUDType.IN_GAME);
        }

        public void StartPauseHUD()
        {
            ActivateHUDS(HUDType.IN_PAUSE);
            GameEvents.Instance.PauseGame();
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene(_menuIndex);
        }

        public void LoadGameScene()
        {
            SceneManager.LoadScene(_gameIndex);
        }

        private void OnInitialize()
        {
            GameEvents.Instance.GameEndedWin += StartWinScreen;
            GameEvents.Instance.GameEndedLose += StartLoseScreen;
            GameEvents.Instance.EnemyReachedTemple += DamageVignetteBlink;

            StartInGameHUD();
        }

        private void StartWinScreen()
        {
            ActivateHUDS(HUDType.WIN_SCREEN);
        }

        private void StartLoseScreen()
        {
            ActivateHUDS(HUDType.LOSE_SCREEN);
        }

        private void ActivateHUDS(HUDType hudType)
        {
            foreach (HUDController hud in _gameHuds)
            {
                if (hud.HudType == hudType)
                {
                    hud.TurnActiveTo(true);
                }
                else
                {
                    hud.TurnActiveTo(false);
                }
            }
        }

        private void DamageVignetteBlink()
        {
            _damageVignette.SetTrigger(_damageVignetteTrigger);
        }

    }
}