﻿namespace BloodyGuardian.UI
{
	public enum HUDType
	{
		IN_GAME = 0,
		IN_PAUSE = 1,
		WIN_SCREEN = 2,
		LOSE_SCREEN = 3
	}
}