﻿using System.Collections;
using UnityEngine;

namespace BloodyGuardian.UI
{
    public class HUDController : MonoBehaviour
    {
        [SerializeField]
        private HUDType _hudType = default;

        public void TurnActiveTo(bool state)
        {
            gameObject.SetActive(state);
        }

        public void TurnOffTimer(float time = 0)
        {
            StartCoroutine(Desactivate());

            IEnumerator Desactivate()
            {
                yield return new WaitForSeconds(time);
                gameObject.SetActive(false);
            }
        }

        public HUDType HudType
        {
            get { return _hudType; }
        }
    }
}
