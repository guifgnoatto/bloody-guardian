﻿using BloodyGuardian.Event;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BloodyGuardian.UI
{
    public class Buttons : MonoBehaviour
    {
       private readonly int _gameSceneIndex = 1;
       
        public void LoadGameScene()
        {
            SceneManager.LoadScene(_gameSceneIndex);
        }

        public void PauseGame()
        {
            GameEvents.Instance.PauseGame();
        }

        public void ResumeGame()
        {
            GameEvents.Instance.ResumeGame();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}