﻿namespace BloodyGuardian.Collision
{
    public interface ICollideable
    {
        void CollideHandler();
    }
}