﻿using BloodyGuardian.IA;
using UnityEngine;
using BloodyGuardian.Manager;

namespace BloodyGuardian.Collision
{
    public class NPCCollider : MonoBehaviour, ICollideable
    {
        public void CollideHandler()
        {
            GetComponent<NPCBase>().TakeDamage();
        }
    }
}