﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BloodyGuardian.Camera
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField]
        private float _magnitude = default;
        [SerializeField]
        private float _duration = default;

        public IEnumerator Shake()
        {
            Vector3 originalPos = transform.localPosition;

            float elapsed = 0.0f;

            while (elapsed < _duration)
            {
                float z = Random.Range(-1f, 1f) * _magnitude;
                float y = Random.Range(-1f, 1f) * _magnitude;

                transform.localPosition = new Vector3(originalPos.x, y, z);

                elapsed += Time.deltaTime;

                yield return null;
            }

            transform.localPosition = originalPos;
        }
    }
}