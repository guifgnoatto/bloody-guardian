﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "New SoundBox Settings", menuName = "Audio/SoundBox Settings")]
public class SoundBoxConfig : ScriptableObject
{
    [Tooltip("The list of soundclips used in this settings. Note that for default a random clip from this list will be selected to play everytime the SB plays a sound" +
             "you can change by checking 'ChangeClipsOnce' in where this config file is being used.")]
    public List<AudioClip> audioClips;
    [Space(20)]
    [Tooltip("for default, if the min and max pitch are different, than it will be randomized. Otherwise, it will use the value of the MaxPitch")]
    public float minPitch = 0.9f, maxPitch = 1.1f;
    [Space(10)]
    [Tooltip("for default, if the min and max Volume are different, than it will be randomized. Otherwise, it will use the value of the MaxVolume")]
    public float minVolume = 0.8f, maxVolume = 1f;
    [Space(10)]
    [Tooltip("The AudioMixer output group that this sound will Play. If empty, the group will be the one of the AudioSource")]
    public AudioMixerGroup outputGroup;


}
