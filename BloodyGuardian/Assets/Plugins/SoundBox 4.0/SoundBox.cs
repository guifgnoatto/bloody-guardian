﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

namespace Triplano.assets.SoundBox
{
    public enum SBState { NONE, INITIALIZED, SETTINGS_LOADED, SOURCE_LOADED, PLAYING, LOOP, STOP, PAUSED, FINISH }
    public enum SBStartAt { METHOD, ENABLE, START }
    [RequireComponent(typeof(AudioSource))]
    [AddComponentMenu("SoundBox/SoundBoxMain")]
    public class SoundBox : MonoBehaviour
    {
        public delegate void OnSetNewSourceHandler();
        public delegate void OnSetNewConfigHandler();

        public OnSetNewSourceHandler onSetNewSource;
        public OnSetNewConfigHandler onSetNewConfig;

        [SerializeField] private SoundBoxConfig _defaultConfig = default;
        [SerializeField] private SBStartAt _startAt = SBStartAt.METHOD;
        [SerializeField] private bool _canBeInterrupted = true;
        [SerializeField] private bool _changeClipOnce = false;
        [SerializeField] private bool _changePitchOnce = false;
        [SerializeField] private bool _changeVolumeOnce = false;

        [Tooltip("The execution Order of the modules attached to this SoundBox")]
        [SerializeField] private List<SoundBoxModule> _modules = new List<SoundBoxModule>();

        private AudioSource _source;
        private SoundBoxConfig _config;
        private bool _isPlaying;
        private bool _finishLoading;

        private SoundSettings _loadedSettings;
        public SBState _state = SBState.NONE;

        private void OnEnable()
        {

            if (_startAt == SBStartAt.ENABLE)
            {
                Initialize();
                PlaySound();
            }
        }

        private void Start()
        {
            if (_state == SBState.NONE)
            {
                Initialize();
            }

            if (_startAt == SBStartAt.START)
            {
                PlaySound();
            }
        }

        public void Initialize()
        {
            _loadedSettings = new SoundSettings(null, 0, 0, -1);
            _source = GetComponent<AudioSource>();
            _source.playOnAwake = false;
            _source.loop = false;
            _finishLoading = false;
            _isPlaying = false;
            UpdateState(SBState.INITIALIZED);
        }


        public void PlaySound()
        {
            if (_isPlaying)
            {
                if (_canBeInterrupted)
                {
                    StopAllCoroutines();
                    StartCoroutine(PlayLoop());
                }
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(PlayLoop());
            }

        }

        public void Stop()
        {
            StopAllCoroutines();

            if (_source)
            {
                _source.Stop();
            }
            else
            {
                Initialize();
                _source.Stop();
            }

            UpdateState(SBState.STOP);
        }

        public void Pause(bool state)
        {
            if (state == true)
            {
                if (_isPlaying)
                {
                    _source.Pause();
                    StopCoroutine(PlayLoop());
                    _isPlaying = false;
                    UpdateState(SBState.PAUSED);
                }
            }
            else
            {
                StartCoroutine(Resume());

                IEnumerator Resume()
                {
                    _source.UnPause();
                    _isPlaying = true;
                    UpdateState(SBState.PLAYING);
                    yield return new WaitForSeconds(_loadedSettings.duration - _source.time);
                    FinishPlaying();

                }
            }

        }

        public void SetConfigAndPlay(SoundBoxConfig newConfig)
        {
            Config = newConfig;
            LoadSettings();
            PlaySound();
        }

        public void SetClipAndPlay(AudioClip newClip)
        {
            LoadSettings();
            Clip = newClip;
            PlaySound();
        }

        private void OnDrawGizmosSelected()
        {
            if (_modules.Count > 0)
            {
                for (int i = _modules.Count - 1; i > -1; i--)
                {
                    if (_modules[i] == null)
                    {
                        _modules.RemoveAt(i);
                    }
                }
            }
        }

        private void UpdateState(SBState newState)
        {
            _state = newState;
            if (_modules.Count > 0)
            {
                foreach (SoundBoxModule module in _modules)
                {
                    if (module.isActiveAndEnabled)
                    {
                        module.UpdateState(newState);
                    }

                }
            }


        }

        private void LoadSettings()
        {
            if (!_source)
            {
                _source = GetComponent<AudioSource>();
            }

            if (!_config)
            {
                _config = _defaultConfig;
            }

            if (_loadedSettings.duration < 0) //if the duration is < 0 this means it is the first time  the loading is happening
            {
                _loadedSettings.clip = _config.audioClips[Random.Range(0, _config.audioClips.Count)];
                _loadedSettings.pitch = Random.Range(_config.minPitch, _config.maxPitch);
                _loadedSettings.duration = (_loadedSettings.clip.length / Mathf.Abs(_loadedSettings.pitch));
                _loadedSettings.volume = Random.Range(_config.minVolume, _config.maxVolume);

            }
            else
            {
                if (!_changeClipOnce)
                {
                    _loadedSettings.clip = _config.audioClips[Random.Range(0, _config.audioClips.Count)];
                }

                if (!_changePitchOnce)
                {

                    _loadedSettings.pitch = Random.Range(_config.minPitch, _config.maxPitch);
                }

                if (!_changeVolumeOnce)
                {

                    _loadedSettings.volume = Random.Range(_config.minVolume, _config.maxVolume);
                }

                _loadedSettings.duration = (_loadedSettings.clip.length / Mathf.Abs(_loadedSettings.pitch));
            }

            UpdateState(SBState.SETTINGS_LOADED);

            LoadSource();
        }


        private void LoadSource()
        {

            if (!_source)
            {
                _source = GetComponent<AudioSource>();
            }

            _source.clip = _loadedSettings.clip;
            _source.volume = _loadedSettings.volume;
            _source.pitch = _loadedSettings.pitch;
            _source.outputAudioMixerGroup = _config.outputGroup;

            UpdateState(SBState.SOURCE_LOADED);
            _finishLoading = true;
        }

        private IEnumerator PlayLoop()
        {

            if (!_finishLoading)
            {
                LoadSettings();
            }

            _isPlaying = true;
            _loadedSettings.duration = Clip.length / Mathf.Abs(_loadedSettings.pitch); // we calculate the Duration right before playing, to have a more accurate value, since this  could be changed by some module.

            UpdateState(SBState.PLAYING);

            _source.Play();
            _finishLoading = false;
            yield return new WaitForSeconds(_loadedSettings.duration * 0.98f);
            FinishPlaying();
        }

        private void FinishPlaying()
        {


            _finishLoading = false;
            _isPlaying = false;
            UpdateState(SBState.FINISH);

            if (_source.loop == true)
            {
                _isPlaying = true;
                _state = SBState.LOOP;
                return;
            }
        }

        public SBState State => _state;

        public bool IsPlaying => _isPlaying;

        public AudioSource Source
        {
            get => _source;
            set
            {
                _source = value;
                onSetNewSource?.Invoke();
            }
        }

        public SoundBoxConfig Config
        {
            get => _config;
            set
            {
                _config = value;
                onSetNewConfig?.Invoke();
            }
        }

        public AudioClip Clip
        {
            get => _loadedSettings.clip;
            set
            {
                _loadedSettings.clip = value;

                if (_source)
                {
                    _source.clip = value;
                }
            }
        }

        public float Pitch
        {
            get => _loadedSettings.pitch;
            set
            {
                _loadedSettings.pitch = value;

                if (_source)
                {
                    _source.pitch = value;
                }
            }
        }

        public float Volume
        {
            get => _loadedSettings.volume;
            set
            {
                _loadedSettings.volume = value;

                if (_source)
                {
                    _source.volume = value;
                }
            }
        }

        public float Duration
        {
            get => _loadedSettings.duration;
        }

        public float RemaningDuration
        {
            get => (_loadedSettings.duration - _source.time);
        }

        public SoundBoxModule AddModule
        {
            set
            {
                if (_modules.Count > 0)
                {
                    if (_modules.Contains(value) == true)
                    {
                        return;
                    }
                }

                Debug.Log("Added moduule " + value);
                _modules.Add(value);
            }
        }

        public SoundBoxModule RemoveModule
        {
            set
            {
                if (_modules.Count > 0)
                {
                    if (_modules.Contains(value) == true)
                    {
                        Debug.Log("Removed moduule " + value);
                        _modules.Remove(value);
                    }
                }
            }
        }

        public struct SoundSettings
        {
            public AudioClip clip;
            public float pitch;
            public float volume;
            public float duration;

            public SoundSettings(AudioClip newclip, float newPitch, float newVolue, float newDuration)
            {
                clip = newclip;
                pitch = newPitch;
                volume = newVolue;
                duration = newDuration;
            }
        }

    }

}

