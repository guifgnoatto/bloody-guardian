﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Triplano.assets.SoundBox
{

    [CustomEditor(typeof(SoundBox))]
    public class SoundBoxEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            SoundBox sb = (SoundBox) target;
            
            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Play"))
            {
                if (sb.State == SBState.NONE)
                {
                    sb.Initialize();        
                }

                sb.PlaySound();
            }
            
            if (GUILayout.Button("Stop"))
            {
                if (sb.State == SBState.NONE)
                {
                    sb.Initialize();        
                }
                
                sb.Stop();
            }
            
            GUILayout.EndHorizontal();
        }
    }

}
