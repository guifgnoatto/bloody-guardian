﻿using System.Collections;
using UnityEngine;

namespace Triplano.assets.SoundBox
{
    public class SB_Module_DestroyAfterPlay : SoundBoxModule
    {
        public void DestroyAfter(float time)
        {
            if (TryGetComponent<AudioSource>(out AudioSource source))
            {
                if (source.loop == true)
                {
                    return;
                }
            }

#if UNITY_EDITOR
            StartCoroutine(WaitForSoundFinish());

            IEnumerator WaitForSoundFinish()
            {
                yield return new WaitForSeconds(time);


                DestroyImmediate(gameObject);
            }

            return;
#endif
            Destroy(gameObject, time);
        }

        public override void OnFinishPlaying()
        {

            if (TryGetComponent<AudioSource>(out AudioSource source))
            {
                if (source.loop == true)
                {
                    return;
                }
            }

#if UNITY_EDITOR

            DestroyImmediate(gameObject);
            return;
#endif
            Destroy(gameObject);
        }
    }
}

