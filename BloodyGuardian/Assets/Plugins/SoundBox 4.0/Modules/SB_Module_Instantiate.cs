﻿using System.Collections;
using System.Collections.Generic;
using Triplano.assets.SoundBox;
using UnityEngine;

namespace Triplano.assets.SoundBox
{
    [RequireComponent(typeof(SoundBox))]
    [AddComponentMenu("SoundBox/Modules/Instantiate")]
    public class SB_Module_Instantiate : SoundBoxModule
    {
        [SerializeField] private Transform _parent = default;
        private AudioSource _instantiatedSource;
        private Transform _instantiatedSourceTransform;
        private SB_Module_DestroyAfterPlay _moduleDestroyAfter;

        public override void OnInitialize()
        {
            attachedSoundBox.Source.enabled = false;
        }

        public override void OnSettingsLoaded()
        {

            CreateInstantiableSource();
            attachedSoundBox.Source = _instantiatedSource;
        }


        public override void OnPlay()
        {
          
            if (_parent)
            {
                _instantiatedSourceTransform.SetParent(_parent);
                _instantiatedSourceTransform.localPosition = Vector3.zero;
            }
            else
            {
                _instantiatedSourceTransform.position = attachedSoundBox.transform.position;
            }

            _instantiatedSource.name = attachedSoundBox.Clip.name;
            _moduleDestroyAfter.DestroyAfter(attachedSoundBox.RemaningDuration);
        }

        private void CreateInstantiableSource()
        {
            _instantiatedSource = new GameObject().AddComponent<AudioSource>();
            _instantiatedSource.playOnAwake = false;
            _instantiatedSource.loop = false;
            _moduleDestroyAfter = _instantiatedSource.gameObject.AddComponent<SB_Module_DestroyAfterPlay>();
            _instantiatedSourceTransform = _instantiatedSource.transform;
        }
    }
}
