﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Triplano.assets.SoundBox
{
    [RequireComponent(typeof(SoundBox))]
    [AddComponentMenu("SoundBox/Modules/PlayOnClipChange")]
    public class SB_Module_PlayOnClipChange : SoundBoxModule
    {
        [SerializeField] private AudioClip _clip = null;
        [SerializeField] private bool _stop;

        private AudioClip _previousClip;
        private bool _play;

        private void Update()
        {
            if (_clip)
            {
                if (_clip != _previousClip)
                {
                    HandlePlay();
                }
            }
            else
            {
                _previousClip = null;
            }

            if (_stop)
            {
              Stop();
            }
        }
        
        public override void OnInitialize()
        {
            _play = false;
            _stop = false;
            _previousClip = null;
        }

        private void HandlePlay()
        {
            _previousClip = _clip;
            attachedSoundBox.SetClipAndPlay(_clip);

        }

        private void Stop()
        {
            _stop = false;
            attachedSoundBox.Stop();
        }


    }

}
