﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Triplano.assets.SoundBox
{
    public abstract class SoundBoxModule : MonoBehaviour
    {
        [Tooltip("The SoundBox on wich this module will act on.")]
        public SoundBox attachedSoundBox;

        public void UpdateState(SBState state)
        {
            switch (state)
            {
                case SBState.INITIALIZED:
                    OnInitialize();
                    break;
                case SBState.SETTINGS_LOADED:
                    OnSettingsLoaded();
                    break;
                case SBState.SOURCE_LOADED:
                    OnSourceLoaded();
                    break;
                case SBState.PLAYING:
                    OnPlay();
                    break;
                case SBState.STOP:
                    OnStop();
                    break;
                case SBState.FINISH:
                    OnFinishPlaying();
                    break;
            }
        }

        private void OnDrawGizmosSelected()
        {
           SubscribeToSoundBox();
        }

        private void Awake()
        {
            SubscribeToSoundBox();
            
            if (!attachedSoundBox)
            {
                this.enabled = false;
                return;
            }
            
        }

        private void SubscribeToSoundBox()
        {
            if (!attachedSoundBox && TryGetComponent<SoundBox>(out SoundBox sb))
            {
                attachedSoundBox = sb;
            }

            if (attachedSoundBox)
            {
                attachedSoundBox.AddModule = this;
            }
        }
        

        public virtual void OnInitialize() { }
        
        public virtual void OnSettingsLoaded() { }
        
        public virtual void OnSourceLoaded() {}
        
        public virtual void OnPlay() { }
        
        public virtual void OnStop() { }
        
        public virtual void OnFinishPlaying() { }
        
    }
}
