﻿using System.Collections;
using System.Collections.Generic;
using Triplano.assets.SoundBox;
using UnityEngine;

namespace Triplano.assets.SoundBox
{
    [RequireComponent(typeof(SoundBox))]
    [AddComponentMenu("SoundBox/Modules/Loop")]
    public class SB_Module_Loop : SoundBoxModule
    {
        [SerializeField] private float _minLoopDelay = default;
        [SerializeField] private float _maxLoopDelay = default;

        private bool _singleClipLoop = false;
        public override void OnSourceLoaded()
        {
            if (attachedSoundBox.Config.audioClips.Count == 1)
            {
                attachedSoundBox.Source.loop = true;
                _singleClipLoop = true;
            }
            else
            {
                _singleClipLoop = false;
            }
        }

        public override void OnFinishPlaying()
        {
            if (_singleClipLoop)
            {
                return;
            }
            
            float delay = Random.Range(_minLoopDelay, _maxLoopDelay);
            StartCoroutine(ApplyLoopDelay(delay));
        }

        private IEnumerator ApplyLoopDelay(float delay)
        {
            yield return new WaitForSeconds(delay);

            attachedSoundBox.PlaySound();
        }
    }
}